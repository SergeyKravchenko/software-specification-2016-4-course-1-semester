<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\SocialAccountService;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{
    protected $redirectTo = '/game';

    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback(SocialAccountService $service)
    {
        $user = $service->createOrGetUser(
            Socialite::driver('google')->user(),
            'google'
        );

        auth()->login($user);

        return redirect()->to($this->redirectTo);
    }
}