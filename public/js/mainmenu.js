function MainMenu(game) {
    this.backgroundImage = {};
    this.newgameBtn = {};
    this.continueBtn = {};
    this.exitBtn = {};

    var self = this;

    this.preload = function() {
        self.backgroundImage = game.load.image('auth-bg', 'assets/background.png');
        self.newgameBtn = game.load.spritesheet('newgame_button', 'assets/buttons/newgame_button_spritesheet.png', 159, 46);
        self.continueBtn = game.load.spritesheet('continue_button', 'assets/buttons/continue_button_spritesheet.png', 177, 46);
        self.exitBtn = game.load.spritesheet('exit_button', 'assets/buttons/exit_button_spritesheet.png', 108, 46);
    }

    this.create = function() {
        self.backgroundImage = game.add.sprite(game.world.centerX, game.world.centerY, 'auth-bg');
        self.backgroundImage.anchor.set(0.5);
        self.newgameBtn = game.add.button(game.world.centerX - 159/2, game.world.centerY - 100, 'newgame_button', testFunction, this, 1, 0);
        self.continueBtn = game.add.button(game.world.centerX - 177/2, game.world.centerY, 'continue_button', testFunction, this, 1, 0);
        self.exitBtn = game.add.button(game.world.centerX - 108/2, game.world.centerY + 100, 'exit_button', testFunction, this, 1, 0);
    }

    this.update = function() {

    }

    function testFunction() {

    }
}