(function () {
  window.LocationCannibalPalace = LocationCannibalPalace

  function moveToRight(location, pussInBoots, cannibal) {
    location.nitros.push(new Nitro(location.game, location, 700, 120))
    location.nitros.push(new Nitro(location.game, location, 1480, 120))
    location.nitros.push(new Nitro(location.game, location, 2200, 120))
    location.nitros.push(new Nitro(location.game, location, 2700, 120))
    location.nitros.push(new Nitro(location.game, location, 3200, 120))

    pussInBoots.hero.body.velocity.x = 200
    pussInBoots.hero.animations.play('rightwalk')
    pussInBoots.direction = 'right'

    cannibal.hero.body.velocity.x = 300
    cannibal.hero.loadTexture('cannibal', 0)
    cannibal.hero.body.setSize(80, 52)
    cannibal.hero.animations.play('rightwalk')

    location.game.time.events.add(
      5 * Phaser.Timer.SECOND,
      Cannibal.prototype.morphToLion,
      cannibal
    )
  }

  function moveToLeft(location, pussInBoots, cannibal) {
    pussInBoots.hero.body.velocity.x = -200
    pussInBoots.hero.animations.play('leftwalk')
    pussInBoots.direction = 'left'

    cannibal.isElephant = false
    cannibal.hero.loadTexture('mice', 0)
    cannibal.hero.animations.add('miceleftwalk', [0, 1, 2, 3], 10, true)
    cannibal.hero.animations.play('miceleftwalk')
    cannibal.hero.body.velocity.x = -300
    cannibal.hero.body.setSize(48, 25)

    location.nitros.push(new Nitro(location.game, location, 4000, 120))
    location.nitros.push(new Nitro(location.game, location, 3000, 120))
    location.nitros.push(new Nitro(location.game, location, 2500, 120))
    location.nitros.push(new Nitro(location.game, location, 2000, 120))
    location.nitros.push(new Nitro(location.game, location, 1500, 120))
    location.nitros.push(new Nitro(location.game, location, 1000, 120))
  }

  function LocationCannibalPalace(game) {
    this.game = game

    this.map = null
    this.layer = null

    this.pussInBoots = null
    this.cannibal = null
    this.nitros = []

    this.keyUp = null

    this.gameIsOver = false
    this.levelIsFinished = false
  }

  LocationCannibalPalace.prototype.preload = function () {
    this.game.load.tilemap(
      'map',
      'assets/locations/cannibal_palace/tiles.json',
      null,
      Phaser.Tilemap.TILED_JSON
    )
    this.game.load.image('tiles', 'assets/locations/forest/tiles.png')
    this.game.load.spritesheet(
      'pussinboots',
      'assets/locations/forest/pussinboots_spritesheet2.png',
      48,
      64,
      16
    )
    this.game.load.spritesheet(
      'cannibal',
      'assets/locations/cannibal_palace/cannibal_spritesheet.png',
      80,
      52,
      11
    )
    this.game.load.spritesheet(
      'lion',
      'assets/locations/cannibal_palace/lion.png',
      95,
      53,
      5
    )
    this.game.load.spritesheet(
      'elephant',
      'assets/locations/cannibal_palace/elephant_spritesheet.png',
      120,
      70,
      5
    )
    this.game.load.spritesheet(
      'mice',
      'assets/locations/cannibal_palace/mice_spritesheet.png',
      48,
      25,
      4
    )
    this.game.load.image(
      'nitro',
      'assets/locations/cannibal_palace/nitro.png',
      50,
      50
    )
  }

  LocationCannibalPalace.prototype.create = function () {
    this.game.stage.backgroundColor = '#66CCFF'

    this.map = this.game.add.tilemap('map')
    this.map.addTilesetImage('tiles', 'tiles')

    this.layer = this.map.createLayer('Layer1')
    this.layer.resizeWorld()
    this.layer.wrap = true

    this.map.setCollision([34, 35, 36, 37], true, 'Layer1')

    this.keyUp = this.game.input.keyboard.addKey(Phaser.Keyboard.UP)

    this.pussInBoots = new PussInBoots(this.game, this)
    this.cannibal = new Cannibal(this.game, this)

    moveToRight(this, this.pussInBoots, this.cannibal)

    makePause(this)
  }

  LocationCannibalPalace.prototype.update = function () {
    this.pussInBoots.update()
    this.cannibal.update()

    this.game.physics.arcade.overlap(
      this.pussInBoots.hero,
      this.cannibal.hero,
      () => {
        if (this.pussInBoots.direction === 'right') {
          if (!this.gameIsOver) {
            this.gameIsOver = true
            alert('Game is over')
            this.game.state.remove(this.game.state.current);
            this.game.state.add('GameField', new GameField(this.game));
            this.game.state.start('GameField');
          }
        } else {
          if (!this.levelIsFinished) {
            this.levelIsFinished = true
            alert('Level is finished')
            this.game.state.remove(this.game.state.current);
            this.game.state.add('GameField', new GameField(this.game));
            this.game.state.start('GameField');
          }
        }
      },
      null,
      this
    )

    this.nitros.forEach(
      (nitro) => {
        this.game.physics.arcade.overlap(
          this.pussInBoots.hero,
          nitro.nitro,
          PussInBoots.prototype.speedUp,
          null,
          this.pussInBoots
        )
        nitro.update()
      }
    )
  }

  function PussInBoots(game, location) {
    this.game = game
    this.location = location

    this.hero = game.add.sprite(500, 210, 'pussinboots')
    this.hero.anchor.setTo(0.5, 0.5)
    this.hero.animations.add(
      'rightwalk',
      [8, 9, 10, 11, 12, 13, 14, 15],
      10,
      true
    )
    this.hero.animations.add('leftwalk', [7, 6, 5, 4, 3, 2, 1, 0], 10, true)
    game.camera.follow(this.hero)
    game.physics.enable(this.hero, Phaser.Physics.ARCADE)
    this.hero.body.bounce.y = 0
    this.hero.body.gravity.y = 1000
    this.hero.body.collideWorldBounds = true
  }

  PussInBoots.prototype.update = function () {
    this.game.physics.arcade.collide(this.hero, this.location.layer)

    if (this.location.keyUp.isDown && this.hero.body.onFloor()) {
      this.hero.body.velocity.y = -500
    }

    // Кот добрался до правой границы уровня.
    if (this.hero.body.x >= this.game.world.width - 50
      && this.direction === 'right') {
      moveToLeft(this.location, this, this.location.cannibal)
    } else if (this.location.cannibal.hero.body.x <= 50
      && this.direction === 'left') {
      moveToRight(this.location, this, this.location.cannibal)
    }
  }

  PussInBoots.prototype.speedUp = function (pussInBootsSprite, nitroSprite) {
    nitroSprite.kill()

    this.hero.body.velocity.x = 450
    if (this.direction === 'left') {
      this.hero.body.velocity.x = -this.hero.body.velocity.x
    }

    this.game.time.events.add(
      Phaser.Timer.SECOND,
      PussInBoots.prototype.setNormalSpeed,
      this
    )
  }

  PussInBoots.prototype.speedDown = function () {
    if (this.direction === 'left') {
      return
    }

    this.hero.body.velocity.x = 50

    this.game.time.events.add(
      Phaser.Timer.SECOND,
      PussInBoots.prototype.setNormalSpeed,
      this
    )

    if (this.location.cannibal.isElephant === true) {
      this.game.time.events.add(
        4 * Phaser.Timer.SECOND,
        PussInBoots.prototype.speedDown,
        this.location.pussInBoots
      )
    }
  }

  PussInBoots.prototype.setNormalSpeed = function () {
    this.hero.body.velocity.x = 200
    if (this.direction === 'left') {
      this.hero.body.velocity.x = -this.hero.body.velocity.x
    }
  }

  function Cannibal(game, location) {
    this.game = game
    this.location = location

    this.isElephant = false

    this.hero = game.add.sprite(200, 240, 'cannibal')
    this.hero.anchor.setTo(0.5, 0.5)
    this.hero.animations.add(
      'rightwalk',
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      10,
      true
    )
    game.physics.enable(this.hero, Phaser.Physics.ARCADE)
    this.hero.body.bounce.y = 0
    this.hero.body.gravity.y = 1000
    this.hero.body.collideWorldBounds = true
  }

  Cannibal.prototype.update = function () {
    this.game.physics.arcade.collide(this.hero, this.location.layer)
  }

  Cannibal.prototype.morphToLion = function () {
    this.hero.loadTexture('lion', 0)
    this.hero.animations.add('lionrightwalk', [0, 1, 2, 3, 4], 10, true)
    this.hero.animations.play('lionrightwalk')

    this.hero.body.velocity.x = 350

    this.game.time.events.add(
      5 * Phaser.Timer.SECOND,
      Cannibal.prototype.morphToElephant,
      this
    )
  }

  Cannibal.prototype.morphToElephant = function () {
    this.isElephant = true

    this.hero.loadTexture('elephant', 0)
    this.hero.animations.add('elephantrightwalk', [0, 1, 2, 3, 4, 5], 10, true)
    this.hero.animations.play('elephantrightwalk')
    this.hero.body.setSize(120, 70)

    this.hero.body.velocity.x = 200

    this.game.time.events.add(
      3 * Phaser.Timer.SECOND,
      PussInBoots.prototype.speedDown,
      this.location.pussInBoots
    )
  }

  function Nitro(game, location, x, y) {
    this.game = game
    this.location = location

    this.nitro = this.game.add.sprite(x, y, 'nitro')
    this.nitro.anchor.setTo(0.5, 0.5)
    this.game.physics.enable(this.nitro, Phaser.Physics.ARCADE)
  }

  Nitro.prototype.update = function () {
    this.game.physics.arcade.collide(this.nitro, this.location.layer)
  }
})()
