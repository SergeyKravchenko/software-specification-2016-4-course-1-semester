const WINDOW_WIDTH = 800
const WINDOW_HEIGHT = 600

function makePause(location) {
  location.keyP = location.game.input.keyboard.addKey(Phaser.Keyboard.P)
  location.keyP.onDown.add(
    () => {
      if (location.game.paused) {
        location.game.paused = false
        location.pauseText.destroy()
      } else {
        location.game.paused = true
        location.pauseText = location.game.add.text(
          location.camera._targetPosition.x - 150,
          WINDOW_HEIGHT - 150,
          'Press P again to continue',
          {font: '30px Arial', fill: '#fff'}
        )
      }
    }
  )
}
