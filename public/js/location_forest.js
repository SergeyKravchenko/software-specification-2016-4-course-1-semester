(function () {
  window.LocationForest = LocationForest

  function LocationForest(game) {
    this.game = game;       // Phaser.Game

    // Карта
    this.map = null;        // Тайловая карта
    this.layer = null;      // Основной слой тайловой карты
    this.bounds = null;     // Слой тайловой карты, в котором обозначены границы для передвижения кроликов

    // Сущности
    this.pussInBoots = null;
    this.rabbits = [];
    this.countOfRabbits = 15;
	this.countRab = 5;
	this.firstRab = 0;

    // Кнопки
    this.cursors = null;    // Стрелки
    this.qKey = null;
    this.wKey = null;
    this.eKey = null;
    this.rKey = null;

    this._time = 60;        // Игровое время
    this.timerId = null;    // Таймер, обновляющий каждую секунду

    // Текст
    this.textCountOfRabbits = null;
    this.textTimer = null;

    // Иконки
    this.iconRabbit = null;
    this.iconClock = null;
    this.iconBag = null;
  }

  /**
   * Загрузка ресурсов для локации "Лес"
   *
   */
  LocationForest.prototype.preload = function () {
    // Подгружаем тайловую карту
    this.game.load.tilemap(
      'map',
      'assets/locations/forest/map.json',
      null,
      Phaser.Tilemap.TILED_JSON
    );
    // Подгружаем набор тайлов
    this.game.load.image('tiles', 'assets/locations/forest/tiles.png');
    // Подгружаем спрайт с анимацией передвижения нашего персонажа
    this.game.load.spritesheet(
      'pussinboots',
      'assets/locations/forest/pussinboots_spritesheet2.png',
      48,
      64,
      16
    );
    // Подгружаем спрайт с анимацией передвижения кролика
    this.game.load.spritesheet(
      'rabbit',
      'assets/locations/forest/rabbit_spritesheet.png',
      35,
      31,
      16
    );
    // Подгружаем спрайт мешка
    this.game.load.image('bag', 'assets/locations/forest/bag.png', 35, 30);
    // Подгружаем спрайт с анимацией "притвориться мертвым" нашего персонажа
    this.game.load.spritesheet(
      'dead_pussinboots',
      'assets/locations/forest/dead_pussinboots_spritesheet.png',
      80,
      64,
      20
    );
    // Подгружаем иконку кролика
    this.game.load.image(
      'icon_rabbit',
      'assets/locations/forest/rabbit.png',
      64,
      75
    );

    
    // Подгружаем иконку часов
    this.game.load.image(
      'icon_clock',
      'assets/locations/forest/clock.png',
      64,
      64
    );
  }

  /**
   *
   *
   */
  LocationForest.prototype.create = function () {
    this.game.stage.backgroundColor = '#66CCFF'; // Фон

    // Добавляем тайловую карту с ключом «map»
    this.map = this.game.add.tilemap('map');
    // Добавляем набор тайлов с ключом «tiles»
    this.map.addTilesetImage('tiles', 'tiles');

    // Добавляем слой Layer1, прописанный в JSON с ключом "name" в объекте, описывающем слои («layers»)
    this.layer = this.map.createLayer('Layer1');
    // Подгоняем размер игрового мира под размеры слоя
    this.layer.resizeWorld();
    this.layer.wrap = true;

    // Добавляем слой Layer2, прописанный в JSON с ключом "name" в объекте, описывающем слои («layers»)
    this.bounds = this.map.createLayer('Layer2');
    // Подгоняем размер игрового мира под размеры слоя
    this.bounds.resizeWorld();
    this.bounds.wrap = true;

    // Устанавливаем коллизии для тайлов с перечисленными номерами
    this.map.setCollision(
      [33, 34, 35, 36, 37, 38, 50, 51, 52, 53, 66, 67, 68, 69, 82, 83, 84, 85, 98, 99, 100, 101],
      true,
      'Layer1'
    );
    this.map.setCollision(2, true, 'Layer2');

    // Кот
    this.pussInBoots = new PussInBoots(this.game, this, 64, this.game.world.centerY - 100);

    // Кролики
    for (var i = 0; i < this.countOfRabbits; i++) {
      this.rabbits.push(new Rabbit(this.game, this, i * 100, this.game.world.centerY - 100, i));
    }

    // Клавиши
    this.cursors = this.game.input.keyboard.createCursorKeys();     // Цепляем горячие клавиши стрелок
    this.qKey = this.game.input.keyboard.addKey(Phaser.Keyboard.Q); // Цепляем q
    this.wKey = this.game.input.keyboard.addKey(Phaser.Keyboard.W); // Цепляем w
    this.eKey = this.game.input.keyboard.addKey(Phaser.Keyboard.E); // Цепляем e
    this.rKey = this.game.input.keyboard.addKey(Phaser.Keyboard.R); // Цепляем r

    this.eKey.onDown.add(PussInBoots.prototype.die, this.pussInBoots);
    this.rKey.onDown.add(PussInBoots.prototype.revive, this.pussInBoots);

    // Убрать действие перечисленных кнопок в браузере
    this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.Q, Phaser.Keyboard.W, Phaser.Keyboard.E, Phaser.Keyboard.R]);

    // Текст
    this.textCountOfRabbits = this.game.add.text(
      120,
      60,
      this.pussInBoots.countOfRabbits
    );
    this.textCountOfRabbits.anchor.set(0.5);
    this.textCountOfRabbits.font = 'Arial';
    this.textCountOfRabbits.fontWeight = 'bold';
    this.textCountOfRabbits.fontSize = 70;
    this.textCountOfRabbits.fill = '#FFFF66';
    this.textCountOfRabbits.fixedToCamera = true;

    this.textTimer = this.game.add.text(
      this.game.camera.width - 60,
      60,
      this._time
    );
    this.textTimer.anchor.set(0.5);
    this.textTimer.font = 'Arial';
    this.textTimer.fontWeight = 'bold';
    this.textTimer.fontSize = 70;
    this.textTimer.fill = '#FFFF66';
    this.textTimer.fixedToCamera = true;

    // Иконки
    // Иконка кролика
    this.iconRabbit = this.game.add.sprite(50, 50, 'icon_rabbit');
    this.iconRabbit.anchor.set(0.5);
    this.iconRabbit.fixedToCamera = true;

    // Иконка часов
    this.iconClock = this.game.add.sprite(
      this.game.camera.width - 155,
      55,
      'icon_clock'
    );
    this.iconClock.anchor.set(0.5);
    this.iconClock.fixedToCamera = true;

    // Обновление времени каждую секунду
    this.timerId = this.game.time.create(false);
    this.timerId.loop(1000, LocationForest.prototype.updateTime, this);
    this.timerId.start();

    makePause(this)
  }

	
  /**
   *
   *
   */
  LocationForest.prototype.update = function () {
    // Определяем столкновение кота с коллизиями основного слоя
    this.game.physics.arcade.collide(this.pussInBoots.sprite, this.layer);

    this.pussInBoots.sprite.body.velocity.x = 0;
//Math.floor(Math.random() * 100) === 1 &&
    if (this.pussInBoots.bag !== null && this.pussInBoots.countOfRabbits > 0 && this.pussInBoots.countOfRabbits < this.countRab) {
      if (Math.floor(Math.random() * 100) === 1 && this.pussInBoots.countOfRabbits >this.firstRab && this.pussInBoots.countOfRabbits < this.countRab) {
        --this.pussInBoots.countOfRabbits
	if(this.pussInBoots.countOfRabbits ){
        var bag = this.pussInBoots.bag
        this.rabbits.push(new Rabbit(
          this.game,
          this,
          bag.sprite.body.x + 10,
          bag.sprite.body.y,
          0
        ))
	}
      }
    }
	

		if(this.pussInBoots.countOfRabbits === this.countRab && this.pussInBoots.bag !== null){ //add 2 seconds each time the bag gets the rabbit
		//for (var i = 0; i < 5; i++) {
		//	 this.rabbits.splice(i, 1);
		//	
		this.countRab += 5;
		this.firstRab += 5;
		this.pussInBoots.catchBag();//auto get bag
	}


    if (!this.pussInBoots.isDead) {               // Если кот не притворился мертвым
      // Передвижение кота влево/вправо, стоп
      if (this.cursors.right.isDown) {
        this.pussInBoots.move('right');
      }
      else if (this.cursors.left.isDown) {
        this.pussInBoots.move('left');
      }
      else {
        this.pussInBoots.stop();
      }

      // Прыжок
      if (this.cursors.up.isDown) {
        this.pussInBoots.jump();
      }

      // Бросание мешка
      if (this.qKey.isDown) {
        if (!this.pussInBoots.bag) {
          this.pussInBoots.throwBag(
            this.pussInBoots.sprite.body.x,
            this.pussInBoots.sprite.body.y
          );
        }
      }

      // Поднятие мешка
      if (this.wKey.isDown) {
        if (this.pussInBoots.bag && this.game.physics.arcade.overlap(
            this.pussInBoots.sprite,
            this.pussInBoots.bag.sprite
          )) {
          this.pussInBoots.catchBag();
        }
      }
    }

    if (this.pussInBoots.bag) {
      // Определяем столкновение мешка с коллизиями основного слоя
      this.game.physics.arcade.collide(this.pussInBoots.bag.sprite, this.layer);
    }

    for (var i = 0; i < this.rabbits.length; i++) {
      // Определяем столкновение очередного кролика с коллизиями слоя №1
      this.game.physics.arcade.collide(this.rabbits[i].sprite, this.layer);

      this.rabbits[i].move();

      // Определяем столкновение очередного кролика с мешком и учитываем, что кот притворился мертвым
      if (this.pussInBoots.bag && this.game.physics.arcade.overlap(
          this.rabbits[i].sprite,
          this.pussInBoots.bag.sprite
        )) {
        if (this.pussInBoots.isDead) {
          this.rabbits[i].jumpToBag();
          this.rabbits.splice(i, 1);
	//this.rabbits.destroy();
	i--;
          this.pussInBoots.countOfRabbits++;
        }
      }
    }

    this.textCountOfRabbits.text = this.pussInBoots.countOfRabbits;
    this.textTimer.text = this._time;
  }

  LocationForest.prototype.render = function () {
    // this.game.debug.bodyInfo(this.pussInBoots.sprite, 32, 32);

    var coordXMouseWorld = this.game.input.mousePointer.x + this.game.camera.x;
    var coordYMouseWorld = this.game.input.mousePointer.y + this.game.camera.y;

  }

  /**
   *
   *
   */
  LocationForest.prototype.updateTime = function () {
    this._time--;

    if (this.countOfRabbits == this.pussInBoots.countOfRabbits) {
      this.timerId.stop();
      this.game.state.remove(this.game.state.current);
      this.game.state.add('GameField', new GameField(this.game));
      this.game.state.start('GameField');
      //alert('Все кролики пойманы. Их количество: '+ this.pussInBoots.countOfRabbits)
    }

    if (this._time == 0) {
      this.timerId.stop();
      this.game.state.remove(this.game.state.current);
      this.game.state.add('GameField', new GameField(this.game));
      this.game.state.start('GameField');
      //alert('Время вышло. Количество пойманных кроликов: ' + this.pussInBoots.countOfRabbits);
    }
  }

  /**
   *
   * @param game
   * @param location
   * @param x
   * @param y
   * @constructor
   */
  function PussInBoots(game, location, x, y) {
    this.game = game;           // Phaser.Game

    this.location = location;   // Локация, которой принадлежит кот

    this.direction = 'right';   // Текущее направление
    this.isDead = false;        // Состояние, мертв ли?

    this.countOfRabbits = 0;    // Число пойманных кроликов

    this.bag = null;            // Мешок
	
    // Отображение в игре
    this.sprite = this.game.add.sprite(x, y, 'pussinboots'); // Добавляем спрайт
    this.sprite.anchor.setTo(0.5, 0.5); // Устанавливаем точку отсчета координат спрайта: середина нашего кота
    // Добавляем анимации для ходьбы в правую сторону, 1, 2, 3 и т.д. - порядковые номера фрагментов анимации в нашем изображении кота
    this.sprite.animations.add(
      'walk_right',
      [8, 9, 10, 11, 12, 13, 14, 15],
      10,
      true
    );
    this.sprite.animations.add('walk_left', [7, 6, 5, 4, 3, 2, 1, 0], 10, true);

    // Камера
    this.game.camera.follow(this.sprite);

    // Физика для героя
    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
    this.sprite.body.bounce.y = 0;
    this.sprite.body.gravity.y = 1000;
    this.sprite.body.collideWorldBounds = true;
  }

  /**
   * Двигаться в заданном направлении
   *
   * @param direction - направление
   */
  PussInBoots.prototype.move = function (direction) {
    this.direction = direction;
    this.sprite.body.velocity.x = direction == 'right' ? 200 : -200;  // Устанавливаем параметр скорости по X
    this.sprite.animations.play('walk_' + direction);                 // Воспроизведение анимации
  }

  /**
   * Остановиться
   *
   */
  PussInBoots.prototype.stop = function () {
    this.sprite.animations.stop();            // Останавливаем анимацию при бездействии
    if (this.direction == 'left') {
      this.sprite.frame = 7;                // Устанавливаем кадр анимации
    } else {
      this.sprite.frame = 8;
    }
  }

  /**
   * Прыгнуть
   *
   */
  PussInBoots.prototype.jump = function () {
    if (this.sprite.body.onFloor()) {
      this.sprite.body.velocity.y = -350;
    }
  }

  /**
   * Бросить мешок
   *
   * @param x - координата x
   * @param y - координата y
   */
  PussInBoots.prototype.throwBag = function (x, y) {
    this.bag = new Bag(this.game, this.location, x, y);
  }

  /**
   * Поднять мешок
   *
   */
  PussInBoots.prototype.catchBag = function () {
    this.bag.sprite.destroy();
    this.bag = null;
  }

  /**
   * Сдохнуть :D
   *
   */
  PussInBoots.prototype.die = function () {
    if (!this.isDead) {
      this.isDead = true;
      this.sprite.loadTexture('dead_pussinboots', 0);
      this.sprite.animations.add(
        'die_right',
        [10, 12, 13, 14, 15, 16, 17, 18, 19],
        10,
        false
      );
      this.sprite.animations.add(
        'die_left',
        [9, 8, 7, 6, 5, 4, 3, 2, 1, 0],
        10,
        false
      );
      if (this.direction == 'left') {
        this.sprite.animations.play('die_left');
      } else {
        this.sprite.animations.play('die_right');
      }
    }
  }

  /**
   * Выйти из состояния "Притвориться мертвым"
   *
   */
  PussInBoots.prototype.revive = function () {
    if (this.isDead) {
      this.isDead = false;
      this.sprite.loadTexture('pussinboots', 0);
    }
  }

  /**
   *
   *
   * @param game
   * @param x
   * @param y
   * @param index
   * @constructor
   */
  var Rabbit = function (game, location, x, y, index) {
    this.game = game;               // Phaser.Game

    this.location = location;       // Локация, которой принадлежит кролик

    this.index = index;             // Номер кролика(индекс)

    this.direction = 'right';       // Текущее направление

    // Отображение в игре
    this.sprite = this.game.add.sprite(x, y, 'rabbit'); // Добавляем спрайт кролика в позицию x,y
    this.sprite.anchor.setTo(0.5, 0.5);                 // Устанавливаем точку отсчета координат спрайта: середина нашего кролика
    // Добавляем анимации для ходьбы в правую сторону, 8, 9, 10 и т.д. - порядковые номера фрагментов анимации в нашем изображении кролика
    this.sprite.animations.add(
      'walk_right',
      [8, 9, 10, 11, 12, 13, 14, 15],
      10,
      true
    );
    this.sprite.animations.add('walk_left', [0, 1, 2, 3, 4, 5, 6, 7], 10, true);

    // Физика для героя
    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
    this.sprite.body.bounce.y = 0;
    this.sprite.body.gravity.y = 400;
    this.sprite.body.collideWorldBounds = true;
  }

  /**
   * Передвигаться между двумя границами
   *
   */
  Rabbit.prototype.move = function () {
    var pussInBoots = this.location.pussInBoots
    if (!pussInBoots.isDead) {
      var diff = pussInBoots.sprite.body.x - this.sprite.body.x

      if (Math.abs(diff) <= 50) {
        if (diff > 0) {
          this.direction = 'left'
        } else {
          this.direction = 'right'
        }
      }
    }

    this.sprite.body.velocity.x = this.direction === 'right' ? 100 : -100
    this.sprite.animations.play('walk_' + this.direction)
    this.changeDirection();
  }

  /**
   * Сменить направление движения
   *
   */
  Rabbit.prototype.changeDirection = function () {
    if (this.sprite.body.blocked.left == true) {
      if (this.sprite.body.x <= 50) {
        this.sprite.body.x = this.game.world.width - 51
        this.sprite.body.y = 350
      } else {
        this.sprite.body.velocity.y = -100
      }
    } else if (this.sprite.body.blocked.right == true) {
      if (this.sprite.body.x >= this.game.world.width - 50) {
        this.sprite.body.x = 51
      } else {
        this.sprite.body.velocity.y = -100
      }
    }
  }

  /**
   * Заскочить в мешок
   *
   */
  Rabbit.prototype.jumpToBag = function () {
    this.sprite.kill();
  }

  /**
   *
   *
   * @param game
   * @param x
   * @param y
   * @constructor
   */
  function Bag(game, location, x, y) {
    this.game = game;

    this.location = location;

    this.sprite = this.game.add.sprite(x, y, 'bag'); // Добавляем спрайт

    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE); // Включаем физику
    this.sprite.body.bounce.y = 0;
    this.sprite.body.gravity.y = 1000;
  }
})()
