function GameField(game) {
    this.game = game;       // Phaser.Game

    //
    this.modalFactory = null;
    this.currentModal = null;

    this.mapBg = null;      // Фоновая карта

    this.kingBtn = {};
    this.forestBtn = {};
    this.fieldBtn = {};
    this.workersBtn = {};
    this.cannibalBtn = {};
}

GameField.prototype.preload = function() {
    this.game.load.spritesheet('king_button', 'assets/buttons/test/king_button.png', 117, 40);
    this.game.load.spritesheet('forest_button', 'assets/buttons/test/forest_button.png', 77, 40);
    this.game.load.spritesheet('field_button', 'assets/buttons/test/field_button.png', 92, 40);
    this.game.load.spritesheet('workers_button', 'assets/buttons/test/workers_button.png', 188, 40);
    this.game.load.spritesheet('cannibal_button', 'assets/buttons/test/cannibal_button.png', 126, 40);
    this.game.load.spritesheet('goto_button', 'assets/gamefield/goto_button.png', 125, 40);
    this.game.load.spritesheet('exchange_button', 'assets/gamefield/exchange_button.png', 144, 40);
    this.game.load.image("mapBg","assets/gamefield/map.jpg");
    this.game.load.image("bgModal","assets/gamefield/bgModal.jpg");
    this.game.load.image("scene1Modal","assets/gamefield/scene1Modal.png");
    this.game.load.image("scene2Modal","assets/gamefield/scene2Modal.png");
    this.game.load.image("scene3Modal","assets/gamefield/scene3Modal.png");
    this.game.load.image("scene2Modal","assets/gamefield/scene2Modal.png");
    this.game.load.image("scene3Modal","assets/gamefield/scene3Modal.png");
    this.game.load.image("scene4Modal","assets/gamefield/scene4Modal.png");
    this.game.load.image("scene5Modal","assets/gamefield/scene5Modal.png");
}

GameField.prototype.create = function() {
    this.game.world.setBounds(0, 0, 800, 600);

    // Фоновая карта
    this.mapBg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'mapBg');
    this.mapBg.anchor.set(0.5);

    this.kingBtn = this.game.add.button(this.game.world.centerX - 117 / 2, this.game.world.centerY - 150, 'king_button', GameField.prototype.showSceneModalKing, this, 0);
    this.forestBtn = this.game.add.button(this.game.world.centerX - 77 / 2, this.game.world.centerY - 75, 'forest_button', GameField.prototype.showSceneModalLocationForest, this, 0);
    this.fieldBtn = this.game.add.button(this.game.world.centerX - 92 / 2, this.game.world.centerY, 'field_button', GameField.prototype.showSceneModalLocationField, this, 0);
    this.workersBtn = this.game.add.button(this.game.world.centerX - 188 / 2, this.game.world.centerY + 75, 'workers_button', GameField.prototype.showSceneModalWorkers, this, 0);
    this.cannibalBtn = this.game.add.button(this.game.world.centerX - 126 / 2, this.game.world.centerY + 150, 'cannibal_button', GameField.prototype.showSceneModalLocationCannibalPalace, this, 0);

    // Инициализация фабрики модальных окон и добавление типа модального окна для сцены
    this.modalFactory = new gameModal(this.game);
    this.modalFactory.createModal({
        type: "sceneModal",
        includeBackground: false,
        modalCloseOnInput: true,
        itemsArr: [
            {
                type: "image",
                content: "bgModal",
                offsetY: 0,
                contentScale: 1
            },
            {
                type: "image",
                content: "scene1Modal",
                offsetY: -60,
                contentScale: 1
            },
            {
                type: "text",
                fontFamily: "Palatino Linotype",
                fontSize: 42,
                color: "0x000033",
                offsetY: -270
            },
            {
                type: "text",
                fontFamily: "Palatino Linotype",
                fontSize: 22,
                color: "0x000033",
                offsetY: 180
            },
            {
                type: "text",
                content: "X",
                fontSize: 52,
                color: "0x000000",
                offsetY: -260,
                offsetX: 340,
                callback: function () {
                    this.modalFactory.hideModal("sceneModal");
                    this.currentModal = null;
                    this.setEnabledButtons(true);
                }.bind(this)
            },
            {
                type: "image",
                content: "goto_button",
                offsetY: 270,
                callback: function () {
                    switch (this.currentModal) {
                        case 1:
                            this.setLocationForest();
                            break;
                        case 2:
                            this.setLocationField();
                            break;
                        case 3:
                            this.setLocationCannibalPalace();
                            break;
                    }
                }.bind(this)
            },
        ]
    });
}

GameField.prototype.update = function() {

}

GameField.prototype.setContentSceneModal = function(bgImg, sceneImg, titleText, descriptionText, buttonImg) {
    this.modalFactory.updateModalValue(bgImg, 'sceneModal', 0);
    this.modalFactory.updateModalValue(sceneImg, 'sceneModal', 1);
    this.modalFactory.updateModalValue(titleText, 'sceneModal', 2);
    this.modalFactory.updateModalValue(descriptionText, 'sceneModal', 3);
    this.modalFactory.updateModalValue(buttonImg, 'sceneModal', 5);
}

GameField.prototype.setEnabledButtons = function(isEnabled) {
    this.kingBtn.input.enabled = isEnabled;
    this.forestBtn.input.enabled = isEnabled;
    this.fieldBtn.input.enabled = isEnabled;
    this.workersBtn.input.enabled = isEnabled;
    this.cannibalBtn.input.enabled = isEnabled;
}

GameField.prototype.showSceneModalLocationForest = function() {
    this.setEnabledButtons(false);
    descriptionText = "Едва только кот получил всё, что ему было надобно, он живо обулся,\n молодецки притопнул, перекинул через плечо мешок и, придерживая\n его за шнурки передними лапами, зашагал в заповедный лес, где водилось\n множество кроликов. А в мешке у него были отруби и заячья капуста...";
    this.setContentSceneModal("bgModal", "scene1Modal", "Локация: Лес", descriptionText, "goto_button");
    this.currentModal = 1;
    this.modalFactory.showModal("sceneModal");
}

GameField.prototype.setLocationForest = function(){
    this.game.state.remove("GameField");
    this.game.state.add('LocationForest', new LocationForest(this.game));
    this.game.state.start('LocationForest', true, false);
}

GameField.prototype.showSceneModalLocationField = function() {
    this.setEnabledButtons(false);
    descriptionText = "Несколько дней спустя кот пошёл на поле и там, \nспрятавшись среди колосьев, опять открыл свой мешок...";
    this.setContentSceneModal("bgModal", "scene2Modal", "Локация: Поле", descriptionText, "goto_button");
    this.currentModal = 2;
    this.modalFactory.showModal("sceneModal");
}

GameField.prototype.setLocationField = function() {
    this.game.state.remove("GameField");
    this.game.state.add('LocationField', new LocationField(this.game));
    this.game.state.start('LocationField', true, false);
}

GameField.prototype.showSceneModalLocationCannibalPalace = function() {
    this.setEnabledButtons(false);
    descriptionText = "И вот, наконец, кот прибежал к воротам прекрасного замка.\n Тут жил один очень богатый великан-людоед. Никто на свете никогда\n не видел великана богаче этого. Все земли, по которым проехала\n королевская карета, были в его владении...";
    this.setContentSceneModal("bgModal", "scene3Modal", "Локация: Дворец Людоеда", descriptionText, "goto_button");
    this.currentModal = 3;
    this.modalFactory.showModal("sceneModal");
}

GameField.prototype.setLocationCannibalPalace = function() {
    this.game.state.remove("GameField");
    this.game.state.add('LocationCannibalPalace', new LocationCannibalPalace(this.game));
    this.game.state.start('LocationCannibalPalace', true, false);
}

GameField.prototype.showSceneModalKing = function() {
    this.setEnabledButtons(false);
    descriptionText = "В разработке...";
    this.setContentSceneModal("bgModal", "scene4Modal", "Дворец Короля", descriptionText, "exchange_button");
    this.modalFactory.showModal("sceneModal");
}

GameField.prototype.showSceneModalWorkers = function() {
    this.setEnabledButtons(false);
    descriptionText = "В разработке...";
    this.setContentSceneModal("bgModal", "scene5Modal", "Жнецы/Косцы", descriptionText, "exchange_button");
    this.modalFactory.showModal("sceneModal");
}

