(function () {
  window.LocationField = LocationField

  function LocationField(game) {
    this.game = game;       // Phaser.Game

    // Карта
    this.map = null;        // Тайловая карта
    this.layer = null;      // Основной слой тайловой карты
    this.bounds = null;     // Слой тайловой карты, в котором обозначены границы для передвижения птицы

    // Сущности
    this.pussInBoots = null;
    this.birds = [];
    this.countOfBirds = 15;

    // Кнопки
    this.cursors = null;    // Стрелки
    this.qKey = null;
    this.wKey = null;
    this.eKey = null;
    this.rKey = null;

    this._time = 60;        // Игровое время
    this.timerId = null;    // Таймер, обновляющий каждую секунду

    // Текст
    this.textCountOfBirds = null;
    this.textTimer = null;

    // Иконки
    this.iconBird = null;
    this.iconClock = null;
  }

  /**
   * Загрузка ресурсов для локации "Поле"
   *
   */
  LocationField.prototype.preload = function () {
    // Подгружаем тайловую карту
    this.game.load.tilemap(
      'map',
      'assets/locations/field/map.json',
      null,
      Phaser.Tilemap.TILED_JSON
    );
    // Подгружаем набор тайлов
    this.game.load.image('tiles', 'assets/locations/field/tiles.png');

    this.game.load.image('tiles2', 'assets/locations/field/country_field.png');
    // Подгружаем спрайт с анимацией передвижения нашего персонажа
    this.game.load.spritesheet(
      'pussinboots',
      'assets/locations/field/pussinboots_spritesheet2.png',
      48,
      64,
      16
    );
    // Подгружаем спрайт с анимацией передвижения птицы
    this.game.load.spritesheet(
      'bird',
      'assets/locations/field/bird_spritesheet.png',
      35,
      32,
      6
    );
    // Подгружаем спрайт мешка
    this.game.load.image('bag', 'assets/locations/field/bag.png', 35, 30);
    // Подгружаем спрайт с анимацией "притвориться мертвым" нашего персонажа
    this.game.load.spritesheet(
      'dead_pussinboots',
      'assets/locations/forest/dead_pussinboots_spritesheet.png',
      80,
      64,
      20
    );
    // Подгружаем иконку птицы
    this.game.load.image('icon_bird', 'assets/locations/field/bird.png', 64, 75);
    // Подгружаем иконку часов
    this.game.load.image(
      'icon_clock',
      'assets/locations/field/clock.png',
      64,
      64
    );
    //load the wheat
    this.game.load.image('wheat', 'assets/locations/field/wheat1.png', 100, 125);
  }

  /**
   *
   *
   */
  LocationField.prototype.create = function () {
    this.game.stage.backgroundColor = '#66CCFF'; // Фон

    // Добавляем тайловую карту с ключом «map»
    this.map = this.game.add.tilemap('map');
    // Добавляем набор тайлов с ключом «tiles»
    this.map.addTilesetImage('tiles', 'tiles');
    this.map.addTilesetImage('tiles2', 'tiles2');

    // Добавляем слой Layer1, прописанный в JSON с ключом "name" в объекте, описывающем слои («layers»)
    this.layer = this.map.createLayer('Layer1');
    // Подгоняем размер игрового мира под размеры слоя
    this.layer.resizeWorld();
    this.layer.wrap = true;

    // Добавляем слой Layer2, прописанный в JSON с ключом "name" в объекте, описывающем слои («layers»)
    this.bounds = this.map.createLayer('Layer2');
    // Подгоняем размер игрового мира под размеры слоя
    this.bounds.resizeWorld();
    this.bounds.wrap = true;

    // Устанавливаем коллизии для тайлов с перечисленными номерами
    this.map.setCollision(
      [33, 34, 35, 36, 37, 38, 50, 51, 52, 53, 66, 67, 68, 69, 82, 83, 84, 85, 98, 99, 100, 101],
      true,
      'Layer1'
    );
    this.map.setCollision(2, true, 'Layer2');

    // Кот
    this.pussInBoots = new PussInBoots(this.game, this, 64, this.game.world.centerY - 100);

    // птицы
    for (var i = 0; i < this.countOfBirds; i++) {
      this.birds.push(new Bird(this.game, this, i * 100, this.game.world.centerY - 100, i));
    }

    // Клавиши
    this.cursors = this.game.input.keyboard.createCursorKeys();     // Цепляем горячие клавиши стрелок
    this.qKey = this.game.input.keyboard.addKey(Phaser.Keyboard.Q); // Цепляем q
    this.wKey = this.game.input.keyboard.addKey(Phaser.Keyboard.W); // Цепляем w
    this.eKey = this.game.input.keyboard.addKey(Phaser.Keyboard.E); // Цепляем e
    this.rKey = this.game.input.keyboard.addKey(Phaser.Keyboard.R); // Цепляем r

    this.eKey.onDown.add(PussInBoots.prototype.die, this.pussInBoots);
    this.rKey.onDown.add(PussInBoots.prototype.revive, this.pussInBoots);

    // Убрать действие перечисленных кнопок в браузере
    this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.Q, Phaser.Keyboard.W, Phaser.Keyboard.E, Phaser.Keyboard.R]);

    // Текст
    this.textCountOfBirds = this.game.add.text(
      120,
      60,
      this.pussInBoots.countOfBirds
    );
    this.textCountOfBirds.anchor.set(0.5);
    this.textCountOfBirds.font = 'Arial';
    this.textCountOfBirds.fontWeight = 'bold';
    this.textCountOfBirds.fontSize = 70;
    this.textCountOfBirds.fill = '#FFFF66';
    this.textCountOfBirds.fixedToCamera = true;

    this.textTimer = this.game.add.text(
      this.game.camera.width - 60,
      60,
      this._time
    );
    this.textTimer.anchor.set(0.5);
    this.textTimer.font = 'Arial';
    this.textTimer.fontWeight = 'bold';
    this.textTimer.fontSize = 70;
    this.textTimer.fill = '#FFFF66';
    this.textTimer.fixedToCamera = true;

    // Иконки
    // Иконка птицы
    this.iconBird = this.game.add.sprite(50, 50, 'icon_bird');
    this.iconBird.anchor.set(0.5);
    this.iconBird.fixedToCamera = true;

    // Иконка часов
    this.iconClock = this.game.add.sprite(
      this.game.camera.width - 155,
      55,
      'icon_clock'
    );
    this.iconClock.anchor.set(0.5);
    this.iconClock.fixedToCamera = true;

    this.wheats = []
    this.wheats.push(new Wheat(this.game, this, 300, 230))
    this.wheats.push(new Wheat(this.game, this, 624, 230))

    // Обновление времени каждую секунду
    this.timerId = this.game.time.create(false);
    this.timerId.loop(1000, LocationField.prototype.updateTime, this);
    this.timerId.start();

    makePause(this)
  }

  /**
   *
   *
   */
  LocationField.prototype.update = function () {
    // Определяем столкновение кота с коллизиями основного слоя
    this.game.physics.arcade.collide(this.pussInBoots.sprite, this.layer);

    this.pussInBoots.sprite.body.velocity.x = 0;
    this.wheats.forEach(function (wheat) {
      wheat.update()
    })

    if (!this.pussInBoots.isDead) {               // Если кот не притворился мертвым
      // Передвижение кота влево/вправо, стоп
      if (this.cursors.right.isDown) {
        this.pussInBoots.move('right');
      }
      else if (this.cursors.left.isDown) {
        this.pussInBoots.move('left');
      }
      else {
        this.pussInBoots.stop();
      }

      // Прыжок
      if (this.cursors.up.isDown) {
        this.pussInBoots.jump();
      }

      // Бросание мешка
      if (this.qKey.isDown) {
        if (!this.pussInBoots.bag) {
          this.pussInBoots.throwBag(
            this.pussInBoots.sprite.body.x,
            this.pussInBoots.sprite.body.y
          );
        }
      }

      // Поднятие мешка
      if (this.wKey.isDown) {
        if (this.pussInBoots.bag && this.game.physics.arcade.overlap(
            this.pussInBoots.sprite,
            this.pussInBoots.bag.sprite
          )) {

          this.pussInBoots.catchBag();

        }
      }
    }

    if (this.pussInBoots.bag) {
      // Определяем столкновение мешка с коллизиями основного слоя
      this.game.physics.arcade.collide(this.pussInBoots.bag.sprite, this.layer);
    }

    for (var i = 0; i < this.birds.length; i++) {
      // Определяем столкновение очередного птицы с коллизиями слоя №1
      this.game.physics.arcade.collide(this.birds[i].sprite, this.layer);
      // Определяем столкновение очередного птицы с коллизиями слоя №2
      this.game.physics.arcade.collide(this.birds[i].sprite, this.bounds);

      this.birds[i].move();

      // Определяем столкновение очередного птицы с мешком и учитываем, что кот притворился мертвым
      if (this.pussInBoots.bag && this.game.physics.arcade.overlap(
          this.birds[i].sprite,
          this.pussInBoots.bag.sprite
        )) {
        //Определить если кот мертый и в пщенице
        if (this.pussInBoots.isDead && this.canPick()) {
          this.birds[i].jumpToBag();//ставить кот в мешоке
          this.birds.splice(i, 1); //splic the birds
          this.pussInBoots.countOfBirds++; //add count to birds caugth

        }
      }
    }

    this.textCountOfBirds.text = this.pussInBoots.countOfBirds;
    this.textTimer.text = this._time;
  }

  LocationField.prototype.canPick = function () {
    return this.wheats.some(function (wheat) {
      return wheat.canpick
    })
  }

  LocationField.prototype.render = function () {
    // this.game.debug.bodyInfo(this.pussInBoots.sprite, 32, 32);

    var coordXMouseWorld = this.game.input.mousePointer.x + this.game.camera.x;
    var coordYMouseWorld = this.game.input.mousePointer.y + this.game.camera.y;

    // this.game.debug.text('Mouse X: ' + coordXMouseWorld, 32, 180, 'rgb(0,0,0)');
    // this.game.debug.text('Mouse Y: ' + coordYMouseWorld, 32, 200, 'rgb(0,0,0)');

    // this.game.debug.text(
    //   'Tile X: ' + this.layer.getTileX(coordXMouseWorld),
    //   32,
    //   140,
    //   'rgb(0,0,0)'
    // );
    // this.game.debug.text(
    //   'Tile Y: ' + this.layer.getTileY(coordYMouseWorld),
    //   32,
    //   160,
    //   'rgb(0,0,0)'
    // );
  }

  /** Update time in gamefield everysecond
   *
   *
   */
  LocationField.prototype.updateTime = function () {
    this._time--;

    if (this.countOfBirds == this.pussInBoots.countOfBirds) {
      this.timerId.stop();
      this.game.state.remove(this.game.state.current);
      this.game.state.add('GameField', new GameField(this.game));
      this.game.state.start('GameField');
    }

    if (this._time == 0) {
      this.timerId.stop();
      this.game.state.remove(this.game.state.current);
      this.game.state.add('GameField', new GameField(this.game));
      this.game.state.start('GameField');

    }
  }

  /**
   *
   * @param game
   * @param location
   * @param x
   * @param y
   * @constructor
   */
  function PussInBoots(game, location, x, y) {
    this.game = game;           // Phaser.Game

    this.location = location;   // Локация, которой принадлежит кот

    this.direction = 'right';   // Текущее направление
    this.isDead = false;        // Состояние, мертв ли?
    this.canpick = false;
    this.countOfBirds = 0;    // Число пойманных птицы

    this.bag = null;            // Мешок

    // Отображение в игре
    this.sprite = this.game.add.sprite(x, y, 'pussinboots'); // Добавляем спрайт
    this.sprite.anchor.setTo(0.5, 0.5); // Устанавливаем точку отсчета координат спрайта: середина нашего кота
    // Добавляем анимации для ходьбы в правую сторону, 1, 2, 3 и т.д. - порядковые номера фрагментов анимации в нашем изображении кота
    this.sprite.animations.add(
      'walk_right',
      [8, 9, 10, 11, 12, 13, 14, 15],
      10,
      true
    );
    this.sprite.animations.add('walk_left', [7, 6, 5, 4, 3, 2, 1, 0], 10, true);

    // Камера
    this.game.camera.follow(this.sprite);

    // Физика для героя
    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
    this.sprite.body.bounce.y = 0;
    this.sprite.body.gravity.y = 1000;
    this.sprite.body.collideWorldBounds = true;
  }

  /**
   * Двигаться в заданном направлении
   *
   * @param direction - направление
   */
  PussInBoots.prototype.move = function (direction) {
    this.direction = direction;
    this.sprite.body.velocity.x = direction == 'right' ? 200 : -200;  // Устанавливаем параметр скорости по X
    this.sprite.animations.play('walk_' + direction);                 // Воспроизведение анимации
  }

  /**
   * Остановиться
   *
   */
  PussInBoots.prototype.stop = function () {
    this.sprite.animations.stop();            // Останавливаем анимацию при бездействии
    if (this.direction == 'left') {
      this.sprite.frame = 7;                // Устанавливаем кадр анимации
    } else {
      this.sprite.frame = 8;
    }
  }

  /**
   * Прыгнуть
   *
   */
  PussInBoots.prototype.jump = function () {
    if (this.sprite.body.onFloor()) {
      this.sprite.body.velocity.y = -350;
    }
  }

  /**
   * Бросить мешок
   *
   * @param x - координата x
   * @param y - координата y
   */
  PussInBoots.prototype.throwBag = function (x, y) {
    if (this.location.canPick() == false)
      this.bag = new Bag(this.game, this.location, x, y);
  }

  /**
   * Поднять мешок
   *
   */
  PussInBoots.prototype.catchBag = function () {
    this.bag.sprite.destroy();
    this.bag = null;
  }

  /**
   * Сдохнуть :D
   *
   */
  PussInBoots.prototype.die = function () {
    if (!this.isDead) {
      this.isDead = true;
      this.sprite.loadTexture('dead_pussinboots', 0);
      this.sprite.animations.add(
        'die_right',
        [10, 12, 13, 14, 15, 16, 17, 18, 19],
        10,
        false
      );
      this.sprite.animations.add(
        'die_left',
        [9, 8, 7, 6, 5, 4, 3, 2, 1, 0],
        10,
        false
      );
      if (this.direction == 'left') {
        this.sprite.animations.play('die_left');
      } else {
        this.sprite.animations.play('die_right');
      }
    }
  }

  /**
   * Выйти из состояния "Притвориться мертвым"
   *
   */
  PussInBoots.prototype.revive = function () {
    if (this.isDead) {
      this.isDead = false;
      this.sprite.loadTexture('pussinboots', 0);
    }
  }

  /**
   *
   *
   * @param game
   * @param x
   * @param y
   * @param index
   * @constructor
   */
  var Bird = function (game, location, x, y, index) {
    this.game = game;               // Phaser.Game

    this.location = location;       // Локация, которой принадлежит птицы

    this.index = index;             // Номер птицы(индекс)

    this.direction = 'right';       // Текущее направление

    // Отображение в игре
    this.sprite = this.game.add.sprite(x, y, 'bird'); // Добавляем спрайт птицы в позицию x,y
    this.sprite.anchor.setTo(0.5, 0.5);                 // Устанавливаем точку отсчета координат спрайта: середина нашего птицы
    // Добавляем анимации для ходьбы в правую сторону, 8, 9, 10 и т.д. - порядковые номера фрагментов анимации в нашем изображении птицы
    this.sprite.animations.add('walk_right', [3, 4, 5], 3, true);
    this.sprite.animations.add('walk_left', [2, 1, 0], 3, true);

    // Физика для героя
    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
    this.sprite.body.bounce.y = 0;
    this.sprite.body.gravity.y = 400;
    this.sprite.body.collideWorldBounds = true;
  }

  /**
   * Передвигаться между двумя границами
   *
   */
  Bird.prototype.move = function () {
    this.sprite.body.velocity.x = this.direction == 'right' ? 100 : -100;  // Устанавливаем параметр скорости по X
    this.sprite.animations.play('walk_' + this.direction);                 // Воспроизведение анимации
    this.changeDirection();
  }

  /**
   * Сменить направление движения
   *
   */
  Bird.prototype.changeDirection = function () {
    if (this.sprite.body.blocked.left == true) {
      this.direction = 'right';
    } else if (this.sprite.body.blocked.right == true) {
      this.direction = 'left'
    }
  }

  /**
   * Заскочить в мешок
   *
   */
  Bird.prototype.jumpToBag = function () {
    //if(this.location.wheat.canpick == true){
    this.sprite.kill();
//	}
  }

  var Wheat = function (game, location, x, y) {
    this.game = game;
    this.location = location;
    this.sprite = this.game.add.sprite(x, y, 'wheat'); // Добавляем спрайт
    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE); // Включаем физику
    this.sprite.body.bounce.y = 0;
    this.sprite.body.gravity.y = 1000;
    this.sprite.body.collideWorldBounds = true;
    //this.hero.body.immovable = true;
  }

  /**
   *
   *
   * @param game
   * @param x
   * @param y
   * @constructor
   */
  function Bag(game, location, x, y) {
    this.game = game;

    this.location = location;

    this.sprite = this.game.add.sprite(x, y, 'bag'); // Добавляем спрайт

    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE); // Включаем физику
    this.sprite.body.bounce.y = 0;
    this.sprite.body.gravity.y = 1000;
  }

  Wheat.prototype.update = function () {
    if (this.game.physics.arcade.overlap(
        this.sprite,
        this.location.pussInBoots.sprite
      ) == true) {
      this.canpick = true;
      console.log('yyy');
    } // Определяем столкновение игрока с коллизиями слоя №2
    else {
      this.canpick = false;
    }
    this.game.physics.arcade.collide(this.sprite, this.location.layer);

  }
})()
