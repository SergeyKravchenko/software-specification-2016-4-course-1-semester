function Starter() {
    this.start = function() {
        var game = new Phaser.Game(
          WINDOW_WIDTH,
          WINDOW_HEIGHT,
          Phaser.CANVAS,
          document.getElementById('game')
        );

        game.state.add('MainMenu', new MainMenu(game));
        game.state.add('GameField', new GameField(game));
        game.state.add('LocationForest', new LocationForest(game));
	game.state.add('LocationField', new LocationField(game));
        game.state.add('LocationCannibalPalace', new LocationCannibalPalace(game));

        game.state.start('GameField');
    }
}

var Starter = new Starter();
Starter.start();
