<?php

Auth::routes();

Route::get('/password/reset', function () {
    abort(404);
});
Route::get('/password/reset/{token}', function () {
    abort(404);
});
Route::post('/password/reset', function () {
    abort(404);
});
Route::post('/password/email', function () {
    abort(404);
});

Route::get('auth/google', 'Auth\GoogleController@redirectToProvider')
    ->name('auth.google');
Route::get(
    'auth/google/callback',
    'Auth\GoogleController@handleProviderCallback'
);

Route::get('/', function () {
    return redirect()->route('game.index');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/game', 'GameController@index')->name('game.index');
});
