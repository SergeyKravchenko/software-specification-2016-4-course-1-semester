@extends('layouts.auth')

@section('content')
  <div class="container">
    <div class="row">
      <div id="game"></div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="/js/common.js"></script>
  <script src="/js/mainmenu.js"></script>
  <script src="/js/gamefield.js"></script>
  <script src="/js/location_forest.js"></script>
  <script src="/js/location_field.js"></script>
  <script src="/js/location_cannibal_palace.js"></script>
  <script src="/js/starter.js"></script>
@endsection
