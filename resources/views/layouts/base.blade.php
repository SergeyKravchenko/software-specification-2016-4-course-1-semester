<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <link rel="stylesheet" href="/vendor/bootstrap/dist/css/bootstrap.css">

  <script>
    window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>
  </script>
</head>

<body>
@yield('body')

<script src="/vendor/jquery/dist/jquery.js"></script>
<script src="/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="/vendor/phaser/build/phaser.js"></script>
<script src="/vendor/phaser-input/build/phaser-input.js"></script>
<script src="/vendor/modal.js"></script>

@yield('scripts')
</body>

</html>
